//
//  HTTPRequest.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public struct HTTPRequest: HTTPMessage {
    public let method: HTTPMethod
    public let uri: URL
    public let version: HTTPVersion

    public let headers: [String: String]

    public let message: String?

    public init(method: HTTPMethod, uri: URL, version: HTTPVersion, headers: [String: String], message: String?) {
        self.method = method
        self.uri = uri
        self.version = version
        self.headers = headers
        self.message = message
    }
}

public extension HTTPRequest {
    var Accept: String? {
        return headers["Accept"]
    }

    var AcceptCharset: String? {
        return headers["Accept-Charset"]
    }

    var AcceptEncoding: String? {
        return headers["Accept-Encoding"]
    }

    var AcceptLanguage: String? {
        return headers["Accept-Language"]
    }

    var Authorization: String? {
        return headers["Authorization"]
    }

    var From: String? {
        return headers["From"]
    }

    var Host: String? {
        return headers["Host"]
    }

    var IfModifiedSince: String? {
        return headers["If-Modified-Since"]
    }

    var IfMatch: String? {
        return headers["If-Match"]
    }

    var IfNoneMatch: String? {
        return headers["If-None-Match"]
    }

    var IfRange: String? {
        return headers["If-Range"]
    }

    var IfUnmodifiedSince: String? {
        return headers["If-Unmodified-Since"]
    }

    var MaxForwards: String? {
        return headers["Max-Forwards"]
    }

    var ProxyAuthorization: String? {
        return headers["Proxy-Authorization"]
    }

    var Range: String? {
        return headers["Range"]
    }

    var Referer: String? {
        return headers["Referer"]
    }

    var UserAgent: String? {
        return headers["User-Agent"]
    }
}

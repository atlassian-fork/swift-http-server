Swift HTTP Server
==============

A non-blocking http server in Swift using [swift-gcd-server](https://bitbucket.org/atlassian/swift-gcd-server).

Usage
======

Sample usage:
```swift
let config = HTTPConfiguration(port: 8080)
let server = HTTPServer(configuration: config)
server.handler = { (uuid: UUID, request: HTTPRequest) -> Void in
    var response = HTTPResponse.ok()

    response.Connection = "keep-alive"
    response.ContentType = "text/plain"

    response.message = "It works!!!"

    server.send(connectionID: uuid, response: response)
}
try server.start()

print("started on \(port)")
RunLoop.current.run()

server.stop()
```

Installation
============

To use swift package manager add the following to your Package.swift:
```swift
        .Package(url: "https://bitbucket.org/atlassian/swift-http-server.git", majorVersion: 0)
```

To use carthage add:
```
git "git@bitbucket.org:atlassian/swift-http-server.git" "master"
```

Other swifty repositories
============
[swift-gcd-server](https://bitbucket.org/atlassian/swift-gcd-server)

[swift-promises](https://bitbucket.org/atlassian/swift-promises)

[swift-sqlite](https://bitbucket.org/atlassian/swift-sqlite)

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
